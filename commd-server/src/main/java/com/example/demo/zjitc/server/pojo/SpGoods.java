package com.example.demo.zjitc.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_goods")
public class SpGoods {
    @TableId(value = "goods_id",type = IdType.AUTO)
    private int goods_id;
    private String goods_name;
    private double goods_price;
    private int goods_number;
    private int goods_weight;
    private int cat_id;
    private String goods_introduce;
    private String goods_big_logo;
    private String goods_small_logo;
    private String is_del;
    private Long add_time;
    private Long upd_time;
    private Long delete_time;
    private int cat_one_id;
    private int cat_two_id;
    private int cat_three_id;
    private int hot_mumber;
    private int is_promote;
    private int goods_state;

    public void setGoodId(int goods_id) {
        this.goods_id = goods_id;
    }

    public void setGoodsName(String goods_name) {
        this.goods_name = goods_name;
    }

    public void setGoodsPrice(double goods_price) {
        this.goods_price = goods_price;
    }

    public void setGoodsNumber(int goods_number) {
        this.goods_number = goods_number;
    }

    public void setGoodsWeight(int goods_weight) {
        this.goods_weight = goods_weight;
    }

    public void setCatId(int cat_id) {
        this.cat_id = cat_id;
    }

    public void setGoodsIntroduce(String goods_introduce) {
        this.goods_introduce = goods_introduce;
    }

    public void setGoodsBigLogo(String goods_big_logo) {
        this.goods_big_logo = goods_big_logo;
    }

    public void setGoodsSmallLogo(String goods_small_logo) {
        this.goods_small_logo = goods_small_logo;
    }

    public void setIsDel(String  is_del) {
        this.is_del = is_del;
    }

    public void setAddTime(Long add_time) {
        this.add_time = add_time;
    }

    public void setUpdTime(Long upd_time) {
        this.upd_time = upd_time;
    }

    public void setDeleteTime(Long delete_time) {
        this.delete_time = delete_time;
    }

    public void setCatOneId(int cat_one_id) {
        this.cat_one_id = cat_one_id;
    }

    public void setCatTwoId(int cat_two_id) {
        this.cat_two_id = cat_two_id;
    }

    public void setCat_three_id(int cat_three_id) {
        this.cat_three_id = cat_three_id;
    }

    public void setHot_mumber(int hot_mumber) {
        this.hot_mumber = hot_mumber;
    }

    public void setIs_promote(int is_promote) {
        this.is_promote = is_promote;
    }

    public void setGoods_state(int goods_state) {
        this.goods_state = goods_state;
    }
}
