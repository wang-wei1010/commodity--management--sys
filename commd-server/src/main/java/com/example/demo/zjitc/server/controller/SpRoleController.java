package com.example.demo.zjitc.server.controller;


import com.example.demo.zjitc.server.common.reponse.ResponseResult;
import com.example.demo.zjitc.server.pojo.SpManager;
import com.example.demo.zjitc.server.pojo.SpRole;
import com.example.demo.zjitc.server.service.impl.SpRoleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  角色前端控制器
 * </p>
 *
 * @author Wang
 * @since 2022-01-01
 */
@RestController
@RequestMapping("/server/sp-role")
public class SpRoleController {

    @Autowired
    private SpRoleServiceImpl spRoleService;

    /**
     * 角色列表查询
     * @return
     */
    @GetMapping("/roles")
    @ResponseBody
    public ResponseResult<List<SpRole>> Roles()
    {
        ResponseResult result = new ResponseResult();
        List<SpRole> roleList = new ArrayList<>();
        try{
            roleList = spRoleService.findRole();
       }catch (Exception e)
       {
           result.setCreateError("查询失败");
           return result;
       }
      result.setCreateSuccess("查询成功",roleList);
      return result;
    }


}

