package com.example.demo.zjitc.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("sp_order")
public class Order {
    @TableId(value = "order_id",type = IdType.AUTO)
    private int order_id;
    private int user_id;
    private String order_number;
    @TableField("order_price")
    private double price;
    private String order_pay;
    private String is_send;
    private String trade_no;
    private String order_fapiao_title;;
    private String order_fapiao_company;
    private String order_fapiao_content;
    private String consignee_addr;
    private String pay_status;
    private int create_time;
    private int update_time;

    public void setOrderId(int order_id) {
        this.order_id = order_id;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }

    public void setOrderNumber(String order_number) {
        this.order_number = order_number;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setOrderPay(String order_pay) {
        this.order_pay = order_pay;
    }

    public void setIsSend(String is_send) {
        this.is_send = is_send;
    }

    public void setTeadeNo(String trade_no) {
        this.trade_no = trade_no;
    }

    public void setOrderFapiaoTitle(String order_fapiao_title) {
        this.order_fapiao_title = order_fapiao_title;
    }

    public void setOrderFapiaoCompany(String order_fapiao_company) {
        this.order_fapiao_company = order_fapiao_company;
    }

    public void setOrderFapiaoContent(String order_fapiao_content) {
        this.order_fapiao_content = order_fapiao_content;
    }

    public void setConsigneeAddr(String consignee_addr) {
        this.consignee_addr = consignee_addr;
    }

    public void setPayStatus(String pay_status) {
        this.pay_status = pay_status;
    }

    public void setCreateTime(int create_time) {
        this.create_time = create_time;
    }

    public void setUpdateTime(int update_time) {
        this.update_time = update_time;
    }
}