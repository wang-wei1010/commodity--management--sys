package com.example.demo.zjitc.server.exception;

public class CategoryException extends Exception{
    public CategoryException() {
        super();
    }

    public CategoryException(String message) {
        super(message);
    }
}
