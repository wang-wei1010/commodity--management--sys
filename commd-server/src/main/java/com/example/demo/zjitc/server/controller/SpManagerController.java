package com.example.demo.zjitc.server.controller;


import cn.hutool.http.HttpResponse;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.zjitc.server.common.JwtTokenUtils;
import com.example.demo.zjitc.server.common.reponse.ResponseResult;
import com.example.demo.zjitc.server.pojo.SpManager;
import com.example.demo.zjitc.server.pojo.SpRole;
import com.example.demo.zjitc.server.pojo.Vo.ManageRoleVo;
import com.example.demo.zjitc.server.service.impl.SpManagerServiceImpl;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 管理员 前端控制器
 * </p>
 *
 * @author Wang
 * @since 2022-01-01
 */

@RequestMapping("/server/sp-manager")
@Controller
public class SpManagerController {

    @Autowired
    private SpManagerServiceImpl spManagerService;

    private final JwtTokenUtils jwtTokenUtils;

    public SpManagerController(JwtTokenUtils jwtTokenUtils) {
        this.jwtTokenUtils = jwtTokenUtils;
    }


    @PostMapping ("/run")
    @ResponseBody
    public String run()
    {
        System.out.println("ce");
          return "测试方法";
    }

    @GetMapping("/run1")
    @ResponseBody
    public String run1(String username,String password)
    {
        System.out.println(username);
        System.out.println("asd");
        Map map = new HashMap();
        map.put("user",username);
        map.put("password",password);
        return jwtTokenUtils.createToken(map);
    }

    @GetMapping("/run2")
    @ResponseBody
    @PreAuthorize("hasPermission('/createGood','c')")
    public String run2()
    {
        return "权限测试";
    }


    /**
     * 用户信息查看（分页）
     * @param query
     * @param pagenum
     * @param pagesize
     * @return
     */
    @GetMapping("/users")
    @ResponseBody
    public ResponseResult getUsers(@RequestParam(required = false) String query,
                                   @RequestParam(value = "pagenum") Integer pagenum,@RequestParam(value = "pagesize") Integer pagesize)
    {
        Page<ManageRoleVo> vo = spManagerService.selectPageVo(pagenum,pagesize);
        ResponseResult result = new ResponseResult();
        result.setSuccess("查询成功",vo);
        return result;
    }

    /**
     * 用户的添加
     * @param manageRoleVo
     * @return
     */
    @PutMapping("/users")
    @ResponseBody
    public ResponseResult addUser(@RequestBody SpManager manageRoleVo)
    {
        System.out.println(manageRoleVo.toString());
        ResponseResult<SpManager> result = new ResponseResult<>();
        SpManager manager = spManagerService.selectByUsername(manageRoleVo.getUsername());
        if (manager!=null)
        {
            result.setCreateError("此用户已经存在！");
            return result;
        }
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        manageRoleVo.setPassword(encoder.encode(manageRoleVo.getPassword()));
        Integer num = spManagerService.addUser(manageRoleVo);
        result.setCreateSuccess("添加成功",manageRoleVo);
        return result;
    }

    /**
     * 用户状态的修改
     * @return
     */
     @GetMapping("/users/{uid}/state/{state}")
     @ResponseBody
     public ResponseResult updateState(@PathVariable(value = "uid") String uid,@PathVariable(value = "state") String state)
    {

      int insert  = spManagerService.updateState(uid,state);
       ResponseResult result = new ResponseResult();
      if (insert!=0)
      {
          result.setSuccess("修改成功");
          return result;
      }
      else {
          result.setSuccess("修改失败");
          return result;
      }
    }

    /**
     * 用户的查询
     * @param id
     * @return
     */
    @GetMapping("/users/{id}")
    @ResponseBody
    public ResponseResult seleUserid(@PathVariable(value = "id") String id)
    {
         SpManager manager =  spManagerService.seleUserid(id);
         ResponseResult<SpManager> managerresult = new ResponseResult<>();
         if (manager!=null)
         {
             managerresult.setSuccess("查询成功",manager);
             return managerresult;
         }
         else {
             managerresult.setError("查询失败");
             return managerresult;
         }
    }

    /**
     * 用户信息的修改
     * @param id
     * @param manager
     * @return
     */
    @PostMapping("/users/{id}")
    @ResponseBody
    public ResponseResult updateUser(@PathVariable(name = "id") String id,@RequestBody SpManager manager)
    {
       int insert= spManagerService.updateUser(id,manager);
      ResponseResult<SpManager> result = new ResponseResult<>();
       if (insert!=0)
      {
           result.setSuccess("修改成功",manager);
           return result;
      }
       else {
           result.setError("修改失败");
           return result;
       }
    }

    /**
     * 用户的删除
     * @param id
     * @return
     */
    @DeleteMapping("/users/{id}")
    @ResponseBody
    public ResponseResult deleteUser(@PathVariable(name = "id") String id)
    {
        int insert= spManagerService.deleteUser(id);
        ResponseResult<SpManager> result = new ResponseResult<>();
        if (insert!=0)
        {
            result.setSuccess("删除成功");
            return result;
        }
        else {
            result.setError("删除失败");
            return result;
        }
    }

    /**
     * 用户角色的设置
     * @param id
     * @param role
     * @return
     */
    @PutMapping("/users/{id}/role")
    @ResponseBody
    public ResponseResult setRole(@PathVariable(name = "id") String id,@RequestBody SpRole role)
    {
        System.out.println(id);
        System.out.println(role.getRoleId());
        int insert =  spManagerService.setRole(id,String.valueOf(role.getRoleId()));
        ResponseResult<SpManager> result = new ResponseResult<>();
        if (insert!=0)
        {
            result.setSuccess("设置角色成功");
            return result;
        }
        else {
            result.setError("设置角色失败");
            return result;
        }
    }

}

