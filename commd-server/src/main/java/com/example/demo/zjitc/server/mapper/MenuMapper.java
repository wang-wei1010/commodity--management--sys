package com.example.demo.zjitc.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.zjitc.server.pojo.Menus;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.awt.*;

/**
 * Author: Wang
 * Date: 2022/1/3 18:50
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menus> {

    @Select("SELECT ps_api_path FROM sp_permission_api WHERE ps_id=#{id}")
    public String selectListFu(int id);
}
