package com.example.demo.zjitc.server.pojo.Vo;

import com.example.demo.zjitc.server.pojo.SpManager;
import lombok.Data;

import java.util.Date;

/**
 * Author: Wang
 * Date: 2022/1/1 15:03
 * 用户角色对象的值继承实体类
 */
@Data
public class ManageRoleVo extends SpManager {
    private String role_name;
    private Date create_time;
    private Boolean mg_state;

    public void setRoleName(String role_name) {
        this.role_name = role_name;
    }

    public void setCreateTime(Date create_time) {
        this.create_time = create_time;
    }

    public void setMgState(Boolean mg_state) {
        this.mg_state = mg_state;
    }
}
