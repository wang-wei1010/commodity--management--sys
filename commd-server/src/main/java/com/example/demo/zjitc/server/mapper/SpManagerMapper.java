package com.example.demo.zjitc.server.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.zjitc.server.pojo.SpManager;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.zjitc.server.pojo.Vo.ManageRoleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 管理员表 Mapper 接口
 * </p>
 *
 * @author Wang
 * @since 2022-01-01
 */
@Mapper
public interface SpManagerMapper extends BaseMapper<SpManager> {

    @Select("SELECT m.mg_id AS id,m.mg_name AS username,m.mg_mobile AS mobile,m.mg_email AS email,FROM_UNIXTIME(m.mg_time,'%Y-%m-%d %H:%i:%s') AS create_time,m.mg_state AS TYPE,m.mg_state AS mg_state,r.role_name AS role_name FROM sp_manager m LEFT JOIN sp_role r ON m.role_id = r.role_id")
    public Page<ManageRoleVo> selectManageVo(Page page);


}

