package com.example.demo.zjitc.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.zjitc.server.mapper.MenuMapper;
import com.example.demo.zjitc.server.pojo.Menus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: Wang
 * Date: 2022/1/3 18:49
 * 权限服务
 */
@Service
public class MenuServiceimpl {

   @Autowired
   private MenuMapper menuMapper;

    public List<Menus> getMenu()
    {
        QueryWrapper<Menus> wrapper = new QueryWrapper<>();
        //父级权限
         wrapper.eq("ps_pid",0);
        List<Menus> pslist = menuMapper.selectList(wrapper);
        //通过父id查询子id
        System.out.println(pslist.toString());
        for (int i=0;i< pslist.size();i++)
        {

            QueryWrapper<Menus> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("ps_pid",pslist.get(i).getPsid());
            wrapper1.eq("ps_level","1");
            List<Menus> pslist1 = menuMapper.selectList(wrapper1);
            //将子权限设置进父权限
            pslist.get(i).setChildren(pslist1);
            //查询子权限的路径
            for (int j=0;j<pslist.get(i).getChildren().size();j++)
            {
              String path = menuMapper.selectListFu(pslist.get(i).getChildren().get(j).getPsid());
                pslist.get(i).getChildren().get(j).setPspath(path);
            }
        }
        return pslist;
    }
}
