package com.example.demo.zjitc.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("sp_goods_attr")
public class goodsAttr {
    @TableId(value = "id",type = IdType.AUTO)
    private Long Id;
    @TableField(value = "goods_id")
    private Integer goodsId;
    @TableField(value = "attr_id")
    private Integer attrId;
    @TableField(value = "attr_value")
    private String attrValue;
    @TableField(value = "add_price")
    private Double addPrice;
}
