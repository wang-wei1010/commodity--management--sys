package com.example.demo.zjitc.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.zjitc.server.mapper.PermissionMapper;
import com.example.demo.zjitc.server.pojo.Permission;
import com.example.demo.zjitc.server.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IPermissionService implements PermissionService {
    @Autowired(required = false)
    private PermissionMapper permissionMapper;

    public List<Permission> list() {
        List<Permission> permissions = permissionMapper.selectList(null);
        return permissions;
    }

    public List<Permission> tree() {
        QueryWrapper<Permission> wrapper=new QueryWrapper<>();
        wrapper.eq("ps_level","0");
        List<Permission> permissionVoList= permissionMapper.selectList(wrapper);
        for(Permission vo:permissionVoList){
            wrapper.clear();
            wrapper.eq("ps_pid",vo.getId());
            List<Permission> childerens= permissionMapper.selectList(wrapper);
            for(Permission child:childerens){
                wrapper.clear();
                wrapper.eq("ps_id",child.getId());
                List<Permission> twochild=permissionMapper.selectList(wrapper);
                child.setChild(twochild);
            }
            vo.setChild(childerens);
        }
        return permissionVoList;
    }
}
