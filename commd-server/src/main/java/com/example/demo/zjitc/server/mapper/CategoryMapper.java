package com.example.demo.zjitc.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.zjitc.server.pojo.Category;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
