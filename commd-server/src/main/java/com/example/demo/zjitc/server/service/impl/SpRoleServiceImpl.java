package com.example.demo.zjitc.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.demo.zjitc.server.mapper.MenuMapper;
import com.example.demo.zjitc.server.pojo.Menus;
import com.example.demo.zjitc.server.pojo.SpRole;
import com.example.demo.zjitc.server.mapper.SpRoleMapper;
import com.example.demo.zjitc.server.service.SpRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *  角色服务实现类
 * </p>
 *
 * @author Wang
 * @since 2022-01-01
 */
@Service
public class SpRoleServiceImpl extends ServiceImpl<SpRoleMapper, SpRole> implements SpRoleService {

    @Autowired
    private SpRoleMapper spRoleMapper;

    @Autowired
    public  MenuMapper menuMapper;

    //角色权限查询
    public List<String> rolejurs(String rolename)
    {
        List<String> list = new ArrayList<>();
        QueryWrapper<SpRole> wrapper = new QueryWrapper<>();
        wrapper.eq("role_name",rolename);
        SpRole spRole = new SpRole();
        try
        {
            spRole =  spRoleMapper.selectOne(wrapper);
        }catch (Exception e)
        {
            return null;
        }
        System.out.println(spRole.toString());
       String str =  spRole.getPsIds();
       String[] strings = str.split(",");
       for (int i=0;i<strings.length;i++)
       {
           list.add(strings[i]);
       }
       return list;
    }

    //角色列表查询
    public List<SpRole> findRole()
    {
        //查询角色信息
        QueryWrapper<SpRole> roleQueryWrapper = new QueryWrapper<>();
       List<SpRole> roleList= spRoleMapper.selectList(roleQueryWrapper);

       //遍历角色
       for (int i=0;i<roleList.size();i++)
       {
           String pids =roleList.get(i).getPsIds()+",";
           String[] roleids = pids.split(",");
       //查询一级权限
           List<Menus> menusListOne = jurRole(roleList.get(i),roleids,0);
       //查询二级权限
           List<Menus> menusListTow = jurRole(roleList.get(i),roleids,1);
       //查询三级权限
           List<Menus> menusListTress = jurRole(roleList.get(i),roleids,2);
       //二级权限封装给一级
           nodeRole(menusListOne,menusListTow);
       //三级封装给二级
           nodeRole(menusListTow,menusListTress);
       //权限设置给角色
       roleList.get(i).setChildren(menusListOne);
       }


      return roleList;
    }

    /**
     * 内部方法（权限查询）
     * @param role
     * @param roleids
     * @param jur
     * @return
     */

    private  List<Menus> jurRole(SpRole role,String[] roleids,Integer jur)
    {
        List<Menus> menusList = new ArrayList<>();
        //遍历角色权限id
        for (int j=0;j<roleids.length;j++)
        {
            //查询一级权限
            if (roleids[j].equals("0")||roleids[j]==null)
            {
                continue;
            }
            QueryWrapper<Menus> menusQueryWrapper = new QueryWrapper<>();
            menusQueryWrapper.eq("ps_id",roleids[j]);
            Menus menus = menuMapper.selectOne(menusQueryWrapper);
            //判断是否是正确权限
            if (menus.getPslevel().equals(jur))
            {
               menusList.add(menus);
            }
        }
        return menusList;
    }

    /**
     * 内部方法（权限拼接）
     * @param menus1
     * @param menus2
     */
    private void nodeRole(List<Menus> menus1,List<Menus> menus2)
    {
        for (Menus parent:menus1)
        {
            List<Menus> list = new ArrayList<>();
            for (Menus node:menus2)
            {
                if (node.getPspid().intValue()==parent.getPsid())
                {
                    list.add(node);
                }
            }
            parent.setChildren(list);
        }
    }



    }


