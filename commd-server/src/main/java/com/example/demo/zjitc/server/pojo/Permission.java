package com.example.demo.zjitc.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
@TableName("sp_permission")
public class Permission {
    @TableId(value = "ps_id",type = IdType.AUTO)
    private int id;
    @TableField("ps_name")
    private String authName;
    @TableField("ps_level")
    private String level;
    @TableField("ps_pid")
    private int pid;
    @TableField("ps_c")
    private String path;
    @TableField(exist = false)
    private List<Permission> child;

}
