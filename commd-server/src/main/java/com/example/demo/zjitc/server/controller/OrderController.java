package com.example.demo.zjitc.server.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.zjitc.server.common.reponse.ResponseResult;
import com.example.demo.zjitc.server.pojo.Order;
import com.example.demo.zjitc.server.service.impl.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/server/sp-manager")
public class OrderController {
    @Autowired
    private IOrderService orderService;
    @GetMapping("/orders")
    public ResponseResult findOrders(@RequestParam int query,
                                     @RequestParam int pagenum,
                                     @RequestParam int pagesize){
        Page<Order> orders = orderService.findOrders(pagenum, pagesize);
        ResponseResult result=new ResponseResult();
        result.setSuccess("查询成功",orders);
        return result;
    }
    @PutMapping("/orders/{id}")
    public ResponseResult up(@PathVariable int id,
                             Order order){
        order.getPrice();
        orderService.up(id,order);
        ResponseResult result=new ResponseResult();
        result.setSuccess("修改成功",order);
        return result;
    }
    @GetMapping("/orders/{id}")
    public ResponseResult find(@PathVariable int id){
        Order order = orderService.find(id);
        ResponseResult result=new ResponseResult();
        result.setSuccess("查询成功",order);
        return result;
    }
}
