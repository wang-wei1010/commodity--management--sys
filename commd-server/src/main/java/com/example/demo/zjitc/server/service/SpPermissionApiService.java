package com.example.demo.zjitc.server.service;

import com.example.demo.zjitc.server.pojo.SpPermissionApi;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Wang
 * @since 2022-01-02
 */
public interface SpPermissionApiService extends IService<SpPermissionApi> {

}
