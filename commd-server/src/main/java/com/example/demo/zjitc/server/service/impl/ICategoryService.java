package com.example.demo.zjitc.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.zjitc.server.exception.CategoryException;
import com.example.demo.zjitc.server.mapper.CategoryAttrMapper;
import com.example.demo.zjitc.server.mapper.CategoryMapper;
import com.example.demo.zjitc.server.pojo.Category;
import com.example.demo.zjitc.server.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ICategoryService implements CategoryService {
    @Autowired(required = false)
    private CategoryMapper categoryMapper;

    @Autowired
    private CategoryAttrMapper categoryAttrMapper;

    public Category get(int id) {
        Category category = categoryMapper.selectById(id);return category;
    }

    public void del(int id) {
        categoryMapper.deleteById(id);
    }

    public void post(Category category) {
        categoryMapper.insert(category);
    }

    @Override
    public List<Category> getListCategory(Integer type) throws CategoryException {
        if(type==null){
            type=3;
        }

        CategoryLevel categoryLevelEnum = CategoryLevel.getCategoryLevelEnum(type);
        if(categoryLevelEnum==null){
            throw new CategoryException("参数错误");
        }
        switch (categoryLevelEnum){
            case LEVEL_ONE:
                return getListOne();
            case LEVEL_TWO:
                return getListTwo();
            case LEVEL_THREE:
                return getListThree();
            default:
                throw new CategoryException("参数错误");
        }

    }

    @Override
    public Page<Category> getPageCategory(Integer type, Integer pagenum, Integer pagesize) throws CategoryException {
        if(type==null){
            type=3;
        }
        Page<Category> page=new Page<>(pagenum,pagesize);
        CategoryLevel categoryLevelEnum = CategoryLevel.getCategoryLevelEnum(type);
        if(categoryLevelEnum==null){
            throw new CategoryException("参数错误");
        }
        switch (categoryLevelEnum){
            case LEVEL_ONE:
                return getPageListOne(page);
            case LEVEL_TWO:
                return getPageListTwo(page);
            case LEVEL_THREE:
                return getPageListThree(page);
            default:
                throw new CategoryException("参数错误");
        }

    }

    private Page<Category> getPageListThree(Page<Category> page) {
        Page<Category> oneLevalPage = getOneLevalPage(page);
        List<Category> listTwo = getCategoryByLevel(CategoryLevel.LEVEL_TWO.LEVEL-1);
        List<Category> listThree = getCategoryByLevel(CategoryLevel.LEVEL_THREE.LEVEL-1);
        node(listTwo,listThree);
        node(oneLevalPage.getRecords(),listTwo);
        return oneLevalPage;

    }

    private Page<Category> getPageListTwo(Page<Category> page) {
        Page<Category> oneLevalPage = getOneLevalPage(page);
        List<Category> listTwo = getCategoryByLevel(CategoryLevel.LEVEL_TWO.LEVEL-1);


        node(oneLevalPage.getRecords(),listTwo);
        return oneLevalPage;
    }

    private Page<Category> getPageListOne(Page<Category> page) {
        Page<Category> oneLevalPage = getOneLevalPage(page);

        return oneLevalPage;
    }

    private Page<Category> getOneLevalPage(Page<Category> page) {
        QueryWrapper<Category> wrapper=new QueryWrapper<>();
        wrapper.eq("cat_level",0);
        Page<Category> categoryPage = (Page<Category>) categoryMapper.selectPage(page, wrapper);
        return categoryPage;
    }

    private List<Category> getListThree() {
        List<Category> oneList = getCategoryByLevel(CategoryLevel.LEVEL_ONE.LEVEL - 1);
        List<Category> twoList = getCategoryByLevel(CategoryLevel.LEVEL_TWO.LEVEL - 1);
        List<Category> threeList = getCategoryByLevel(CategoryLevel.LEVEL_THREE.LEVEL - 1);
        node(twoList,threeList);
        node(oneList,twoList);
        return oneList;
    }

    private List<Category> getListTwo() {
        List<Category> oneList = getCategoryByLevel(CategoryLevel.LEVEL_ONE.LEVEL - 1);
        List<Category> twoList = getCategoryByLevel(CategoryLevel.LEVEL_TWO.LEVEL - 1);
        node(oneList,twoList);
        return oneList;
    }

    private void node(List<Category> oneList, List<Category> twoList) {
        for (Category category1:oneList) {
            List<Category> categories=new ArrayList<>();
            for (Category category2:twoList) {
                if(category1.getCat_id()==category2.getCat_pid()){
                    categories.add(category2);
                }
            }
            category1.setChildren(categories);
        }
    }

    private List<Category> getListOne() {
        List<Category> categoryByLevel = getCategoryByLevel(CategoryLevel.LEVEL_ONE.LEVEL - 1);
        return categoryByLevel;
    }

    private List<Category> getCategoryByLevel(Integer level) {
        QueryWrapper<Category> wrapper=new QueryWrapper<>();
        wrapper.eq("cat_level",level);
        List<Category> categories = categoryMapper.selectList(wrapper);
        return categories;
    }


    public static enum CategoryLevel{
        LEVEL_ONE(1,"一级权限"),
        LEVEL_TWO(2,"二级权限"),
        LEVEL_THREE(3,"三级权限");
        private final Integer LEVEL;
        private final String MESSAGE;

        CategoryLevel(Integer LEVEL, String MESSAGE) {
            this.LEVEL = LEVEL;
            this.MESSAGE = MESSAGE;
        }
        public static CategoryLevel getCategoryLevelEnum(Integer type){
            for (CategoryLevel categoryLevel:CategoryLevel.values()) {
                if(categoryLevel.LEVEL.intValue()==type.intValue()){
                    return categoryLevel;
                }
            }
            return null;
        }
    }
}