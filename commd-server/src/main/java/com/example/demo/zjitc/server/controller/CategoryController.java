package com.example.demo.zjitc.server.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.zjitc.server.common.reponse.ResponseResult;
import com.example.demo.zjitc.server.exception.CategoryException;
import com.example.demo.zjitc.server.pojo.Category;
import com.example.demo.zjitc.server.service.impl.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/server/sp-manager")
public class CategoryController {
    ResponseResult responseResult=new ResponseResult();
    @Autowired
    private ICategoryService categoryService;
    @GetMapping("/categories")
    public ResponseResult q(@RequestParam(required = false) Integer type,
                            @RequestParam(required = false) Integer pagenum,
                            @RequestParam(required = false) Integer pagesize) throws CategoryException {
        ResponseResult result=new ResponseResult();
        try {
            if (pagenum==null||pagesize==null){
                List<Category> list = categoryService.getListCategory(type);
                result.setSuccess("查询成功",list);
            }else {
                Page<Category> pageCategory = categoryService.getPageCategory(type, pagenum, pagesize);
                result.setSuccess("查询成功",pageCategory);
            }
        } catch (Exception e) {
            result.setCreateERROR(e.getMessage());
        }
        return result;


    }
    @PostMapping("/categories")
    public ResponseResult post(@RequestBody Category category){
        categoryService.post(category);
        ResponseResult responseResult=new ResponseResult();
        responseResult.setSuccess("创建成功",category);
        return responseResult;
    }
    @GetMapping("/categories/{id}")
    public ResponseResult get(@PathVariable int id){
        Category category = categoryService.get(id);
        ResponseResult responseResult=new ResponseResult();
        responseResult.setSuccess("查询成功",category);
        return responseResult;

    }
    @DeleteMapping("/categories/{id}")
    public ResponseResult del(@PathVariable int id){
        categoryService.del(id);
        responseResult.setSuccess("删除成功");
        return responseResult;
    }

    @GetMapping("/categories/{id}/attributes")
    public ResponseResult get(@PathVariable int id,
                              @RequestParam(required = false) Integer sel){
        Category category = categoryService.get(id);
        ResponseResult responseResult=new ResponseResult();
        responseResult.setSuccess("查询成功",category);
        return responseResult;

    }
}