package com.example.demo.zjitc.server.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.zjitc.server.common.reponse.ResponseResult;
import com.example.demo.zjitc.server.pojo.SpGoods;
import com.example.demo.zjitc.server.service.impl.SpGoodsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.sound.midi.SysexMessage;

@RestController
@RequestMapping("/server/sp-manager")
public class SpGoodsController {
    @Autowired
    private SpGoodsServiceImpl spGoodsService;

    @GetMapping("/goods")
    public ResponseResult findGoods(@RequestParam int query,
                                    @RequestParam(required = true) int pagenum,
                                    @RequestParam(required = true) int pagesize){
        Page<SpGoods> findgoods = spGoodsService.findgoods(pagenum, pagesize);
        ResponseResult result=new ResponseResult();
        result.setSuccess("查询成功",findgoods);
        return result;
    }
    @PostMapping("/goods")
    public ResponseResult addGood(@RequestBody SpGoods good){
        System.out.println(good);
        spGoodsService.addgood(good);
        ResponseResult result=new ResponseResult();
        result.setSuccess("创建成功",good);
        return result;
    }
    @GetMapping("/goodsas/{id}")
    public ResponseResult findOne(@PathVariable int id){
        System.out.println("商品");
        SpGoods findgood = spGoodsService.findgood(id);
        ResponseResult result =new ResponseResult();
        result.setSuccess("查询成功",findgood);
        return result;
    }
//    @PutMapping("/goods/{id}")
//    public ResponseResult up(@PathVariable int id,
//                             SpGoods good){
//        spGoodsService.up(id,good);
//        ResponseResult result=new ResponseResult();
//        result.setSuccess("修改成功",good);
//        return result;
//    }
    @DeleteMapping("/goods/{id}")
    public ResponseResult del(@PathVariable int id){
        spGoodsService.del(id);
        ResponseResult result=new ResponseResult();
        result.setSuccess("删除成功");
        return result;
    }
}
