package com.example.demo.zjitc.server.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.zjitc.server.mapper.OrderMapper;
import com.example.demo.zjitc.server.pojo.Order;
import com.example.demo.zjitc.server.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IOrderService implements OrderService {
    @Autowired(required = false)
    private OrderMapper orderMapper;
    public Page<Order> findOrders(int pagenum, int pagesize) {
        Page<Order> page=new Page<>(pagenum,pagesize);
        Page<Order> page2 = (Page<Order>) orderMapper.selectPage(page, null);
        return page2;
    }

    public void up(int id, Order order) {
        order.setOrderId(id);
        order.getPrice();
        orderMapper.updateById(order);
    }

    public Order find(int id) {
        Order order = orderMapper.selectById(id);
        return order;
    }
}
