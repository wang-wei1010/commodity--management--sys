package com.example.demo.zjitc.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jdk.dynalink.linker.LinkerServices;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Wang
 * @since 2022-01-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SpRole对象", description="")
public class SpRole implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "role_id", type = IdType.AUTO)
    private Integer roleId;

    @TableField(value = "role_name")
    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @TableField(value = "ps_ids")
    @ApiModelProperty(value = "权限ids,1,2,5")
    private String psIds;

    @TableField(value = "ps_ca")
    @ApiModelProperty(value = "控制器-操作,控制器-操作,控制器-操作")
    private String psCa;

    @TableField(value = "role_desc")
    private String roleDesc;

    @TableField(exist = false)
    private List<Menus> children ;


}
