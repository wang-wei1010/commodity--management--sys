package com.example.demo.zjitc.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.zjitc.server.mapper.SpRoleMapper;
import com.example.demo.zjitc.server.pojo.SpManager;
import com.example.demo.zjitc.server.mapper.SpManagerMapper;
import com.example.demo.zjitc.server.pojo.SpRole;
import com.example.demo.zjitc.server.pojo.Vo.ManageRoleVo;
import com.example.demo.zjitc.server.service.SpManagerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 管理员表 服务实现类
 * </p>
 *
 * @author Wang
 * @since 2022-01-01
 */
@Service
public class SpManagerServiceImpl extends ServiceImpl<SpManagerMapper, SpManager> implements SpManagerService, UserDetailsService {

    @Autowired
    private SpManagerMapper spManagerMapper;

    @Autowired
    private SpRoleMapper spRoleMapper;

    //密码的加密方式
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 安全框架权限验证方法
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //角色集合
        List<GrantedAuthority> authorities = new ArrayList<>();
       //通过用户名从数据库获取信息
        QueryWrapper<SpManager> wrapper = new QueryWrapper<>();
        wrapper.eq("mg_name",username);
        SpManager spManager =  spManagerMapper.selectOne(wrapper);
        if (spManager==null)
        {
            throw new UsernameNotFoundException("用户不存在");
        }

        else {
            //获取用户角色信息
            int roleid = spManager.getRole_id();
            //此用户为超级管理
            if (roleid==0)
            {
                System.out.println(username);
                authorities.add(new SimpleGrantedAuthority("ROLE_"+"superadmin"));
                String password = spManager.getPassword();
                System.out.println(password);
                System.out.println("----------------");
                return new User(username,password,authorities);
            }
            //获取响应角色信息
            else {
                QueryWrapper<SpRole> wrapper1 = new QueryWrapper<>();
                wrapper1.eq("role_id",roleid);
                SpRole spRole =  spRoleMapper.selectOne(wrapper1);
                authorities.add(new SimpleGrantedAuthority("ROLE_"+spRole.getRoleName()));
            }
        }
        System.out.println("asdsadasd");
        return new User(spManager.getUsername(),spManager.getPassword(),authorities);
    }



    public Page<ManageRoleVo> selectPageVo(Integer pagenum,Integer pagesize)
    {
        Page<ManageRoleVo> page = new Page<>(pagenum,pagesize);
        page = spManagerMapper.selectManageVo(page);
        List<ManageRoleVo> managerList = page.getRecords();
        for (int i=0;i<managerList.size();i++)
        {
            if (managerList.get(i).getRole_name()==null)
            {
                managerList.get(i).setRole_name("超级管理员");
            }
        }
        System.out.println(page.toString());
        return page;
    }


    public SpManager selectByUsername(String username)
    {
       QueryWrapper<SpManager> wrapper = new QueryWrapper<>();
       wrapper.eq("mg_name",username);
       SpManager manager =  spManagerMapper.selectOne(wrapper);
        return manager;
    }


    public Integer addUser(SpManager manageRoleVo)
    {
       System.out.println(manageRoleVo.toString());
       //设置需要的默认字段
        Date date = new Date();
        manageRoleVo.setRoleId(0);
        manageRoleVo.setType(1);
        manageRoleVo.setMgTime((int)(date.getTime()/1000));
        int insert = spManagerMapper.insert(manageRoleVo);
        manageRoleVo.setPassword(null);
        return insert;
    }

    public Integer updateState(String uid,String state)
    {
      UpdateWrapper<SpManager> wrapper = new UpdateWrapper<>();
      wrapper.eq("mg_id",uid);
      wrapper.set("mg_state",state);
      int insert =  spManagerMapper.update(null,wrapper);
      return insert;
    }


    public SpManager seleUserid(String id)
    {
        QueryWrapper<SpManager> wrapper = new QueryWrapper<>();
        wrapper.eq("mg_id",id);
        SpManager manager = spManagerMapper.selectOne(wrapper);
         return manager;
    }

    public Integer updateUser(String id, SpManager manager)
    {
        UpdateWrapper<SpManager> wrapper = new UpdateWrapper<>();
        wrapper.eq("mg_id",id);
        wrapper.set("mg_mobile",manager.getMobile());
        wrapper.set("mg_email",manager.getEmail());
        int insert =  spManagerMapper.update(null,wrapper);
        return insert;
    }


    public Integer deleteUser(String id)
    {
       int insert =  spManagerMapper.deleteById(id);
       return insert;
    }

    public Integer setRole(String id,String role)
    {
        System.out.println(role);
        UpdateWrapper<SpManager> wrapper = new UpdateWrapper<>();
        wrapper.eq("mg_id",id);
        wrapper.set("role_id",role);
        int insert = spManagerMapper.update(null,wrapper);
        return insert;
    }

}
