package com.example.demo.zjitc.server.service;

import com.example.demo.zjitc.server.pojo.SpManager;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理员表 服务类
 * </p>
 *
 * @author Wang
 * @since 2022-01-01
 */
public interface SpManagerService extends IService<SpManager> {

}
