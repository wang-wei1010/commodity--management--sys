package com.example.demo.zjitc.server.controller;

import com.example.demo.zjitc.server.common.reponse.ResponseResult;
import com.example.demo.zjitc.server.pojo.Permission;
import com.example.demo.zjitc.server.service.impl.IPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/server/sp-manager")
public class PermissController {
    @Autowired
    private IPermissionService permissionService;
    @GetMapping("/rights/{type}")
    public ResponseResult right(@PathVariable String type){
        ResponseResult responseResult=new ResponseResult();
        if (type.equals("list")){
            List<Permission> list = permissionService.list();
            responseResult.setSuccess("查询成功",list);
            return responseResult;
        }if (type.equals("tree")){
            List<Permission> list = permissionService.tree();
            responseResult.setSuccess("查询成功",list);
            return responseResult;
        }
        return null;
    }
}
