package com.example.demo.zjitc.server.mapper;

import com.example.demo.zjitc.server.pojo.SpRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Wang
 * @since 2022-01-01
 */
@Mapper
public interface SpRoleMapper extends BaseMapper<SpRole> {

}
