package com.example.demo.zjitc.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.zjitc.server.pojo.goodsAttr;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CategoryAttrMapper extends BaseMapper<goodsAttr> {
}
