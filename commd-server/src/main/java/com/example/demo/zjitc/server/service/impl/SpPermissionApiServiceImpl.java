package com.example.demo.zjitc.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.zjitc.server.pojo.SpPermissionApi;
import com.example.demo.zjitc.server.mapper.SpPermissionApiMapper;
import com.example.demo.zjitc.server.service.SpPermissionApiService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  角色权限~~服务实现类
 * </p>
 *
 * @author Wang
 * @since 2022-01-02
 */
@Service
public class SpPermissionApiServiceImpl extends ServiceImpl<SpPermissionApiMapper, SpPermissionApi> implements SpPermissionApiService {

    @Autowired
    private SpPermissionApiMapper spPermissionApiMapper;

    //方法权限id查询
    public int jurmethof(String method)
    {
        System.out.println(method);
        QueryWrapper<SpPermissionApi> wrapper = new QueryWrapper<>();
        wrapper.eq("ps_api_action",method);
        SpPermissionApi spPermissionApi = new SpPermissionApi();
        try
        {
             spPermissionApi =spPermissionApiMapper.selectOne(wrapper);
        }catch (Exception e)
        {
            return -1;
        }
        return spPermissionApi.getPsId();

    }


}
