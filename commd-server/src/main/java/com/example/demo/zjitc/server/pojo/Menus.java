package com.example.demo.zjitc.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.models.auth.In;
import lombok.Data;

import java.util.List;

/**
 * Author: Wang
 * Date: 2022/1/3 18:37
 * 权限管理
 */
@Data
@TableName("sp_permission")
public class Menus {
    @TableId(value = "ps_id",type = IdType.AUTO)
    private Integer psid;

    @TableField(  "ps_name")
    private String psname;

    @TableField( "ps_pid")
    private Integer pspid;

    @TableField( "ps_c")
    private String psc;

    @TableField( "ps_a")
    private String psa;

    @TableField( "ps_level")
    private Integer pslevel;

    @TableField(exist = false)
    private String pspath;

    @TableField(exist = false)
    private List<Menus> children;


}
