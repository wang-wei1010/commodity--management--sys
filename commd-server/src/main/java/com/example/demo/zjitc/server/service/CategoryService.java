package com.example.demo.zjitc.server.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.zjitc.server.exception.CategoryException;
import com.example.demo.zjitc.server.pojo.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getListCategory(Integer type)  throws CategoryException;

    Page<Category> getPageCategory(Integer type, Integer pagenum, Integer pagesize)  throws CategoryException;

}
