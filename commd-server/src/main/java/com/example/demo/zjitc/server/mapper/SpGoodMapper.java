package com.example.demo.zjitc.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.zjitc.server.pojo.SpGoods;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SpGoodMapper extends BaseMapper<SpGoods> {
}
