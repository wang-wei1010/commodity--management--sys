package com.example.demo.zjitc.server.common.reponse;

import com.example.demo.zjitc.server.pojo.Role;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.ALWAYS)
public class ResponseResult<T> implements Serializable {
    private T data;
    private Meta meta;

    public ResponseResult(T data, Meta meta) {
        this.data = data;
        this.meta = meta;
    }

    public ResponseResult() {
       this.meta = new Meta();
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    //请求成功
    public void setSuccess(){
        meta.setStatus(ResonseCode.SUCCESS.getCode());
        meta.setMsg(ResonseCode.SUCCESS.getMessage());
    }

    public void setSuccess(String msg){
        meta.setStatus(ResonseCode.SUCCESS.getCode());
        meta.setMsg(msg);
    }
    public void setSuccess(String msg,T data){
        meta.setStatus(ResonseCode.SUCCESS.getCode());
        meta.setMsg(msg);
        this.data = (T) data;
    }

    //创建成功
    public void setCreateSuccess(){
        meta.setStatus(ResonseCode.CREATED.getCode());
        meta.setMsg(ResonseCode.CREATED.getMessage());
    }

    public void setCreateSuccess(String msg){
        meta.setStatus(ResonseCode.CREATED.getCode());
        meta.setMsg(msg);
    }
    public void setCreateSuccess(String msg,T data){
        meta.setStatus(ResonseCode.CREATED.getCode());
        meta.setMsg(msg);
        this.data = data;
    }
    //失败
    public void setError(){
        meta.setStatus(ResonseCode.INTERNAL_SERVER_ERROR.getCode());
        meta.setMsg(ResonseCode.INTERNAL_SERVER_ERROR.getMessage());
    }

    public void setError(String msg){
        meta.setStatus(ResonseCode.INTERNAL_SERVER_ERROR.getCode());
        meta.setMsg(msg);
    }
    public void setError(String msg,T data){
        meta.setStatus(ResonseCode.INTERNAL_SERVER_ERROR.getCode());
        meta.setMsg(msg);
        this.data = data;
    }

    public void setCreateError(String msg) {
        meta.setStatus(ResonseCode.Unprocesable_Entity.getCode());
        meta.setMsg(msg);
    }

    public void setCreateERROR(String msg){
        meta.setStatus(ResonseCode.Unprocesable_Entity.getCode());
        meta.setMsg(msg);
    }

    public void setCreatedSuccess(String msg,T data){
        meta.setStatus(ResonseCode.CREATED.getCode());
        meta.setMsg(msg);
        this.data=data;
    }
}

class Meta{
    private Integer status;
    private String msg;

    public Meta() {
    }

    public Meta(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
