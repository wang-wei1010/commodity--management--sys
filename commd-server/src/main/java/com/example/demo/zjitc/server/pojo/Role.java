package com.example.demo.zjitc.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
@TableName("sp_role")
public class Role {
    @TableId(value = "role_id",type = IdType.AUTO)
    private int roleId;
    private String roleName;
    private String roleDesc;
    private String ps_ids;

    public String getPsIds() {
        return ps_ids;
    }

    public void setPsIds(String ps_ids) {
        this.ps_ids = ps_ids;
    }
    @TableField(exist = false)
    public List<Permission> children;
}
