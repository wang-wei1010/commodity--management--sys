package com.example.demo.zjitc.server.controller;

import com.example.demo.zjitc.server.common.reponse.ResponseResult;
import com.example.demo.zjitc.server.pojo.Role;
import com.example.demo.zjitc.server.service.impl.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/server/sp-manager")
public class RolerController {
    @Autowired
    private IRoleService roleService;

    @GetMapping("/roles")
    public ResponseResult get(){
        List<Role> roles = roleService.get();
        ResponseResult responseResult=new ResponseResult();
        responseResult.setSuccess("查询成功",roles);
        return responseResult;
    }
    @PostMapping("/roles")
    public ResponseResult add(Role role){
        roleService.add(role);
        ResponseResult result=new ResponseResult();
        result.setCreatedSuccess("添加成功",role);
        return result;
    }
    @GetMapping("/roles/{id}")
    public ResponseResult find(@PathVariable int id){
        Role role = roleService.find(id);
        ResponseResult result=new ResponseResult();
        result.setSuccess("查询成功",role);
        return result;
    }
    @PutMapping("/roles/{id}")
    public ResponseResult up(@PathVariable int  id,
                             Role role){
        roleService.up(id,role);
        ResponseResult result=new ResponseResult();
        result.setSuccess("修改成功",role);
        return result;
    }
    @DeleteMapping("/roles/{id}")
    public ResponseResult del(@PathVariable int id){
        roleService.del(id);
        ResponseResult result=new ResponseResult();
        result.setSuccess("删除成功");
        return result;
    }
    @PostMapping("/roles/{id}/rights")
    public ResponseResult uprole(@PathVariable int id,
                                 @RequestParam String rids){
        roleService.uprole(id,rids);
        ResponseResult result =new ResponseResult();
        result.setSuccess("修改成功");
        return  result;
    }
}
