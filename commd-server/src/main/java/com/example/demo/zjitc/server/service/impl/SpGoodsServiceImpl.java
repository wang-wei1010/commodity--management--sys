package com.example.demo.zjitc.server.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.zjitc.server.mapper.SpGoodMapper;
import com.example.demo.zjitc.server.pojo.SpGoods;
import com.example.demo.zjitc.server.service.SpGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpGoodsServiceImpl implements SpGoodsService {
    @Autowired
    private SpGoodMapper spGoodMapper;

    public Page<SpGoods> findgoods(int pagenum, int pagesize) {
        Page<SpGoods> page=new Page<>(pagenum,pagesize);
        Page<SpGoods> page1 = (Page<SpGoods>) spGoodMapper.selectPage(page, null);
        return page1;
    }

    public void addgood(SpGoods good) {
        good.setAddTime(System.currentTimeMillis()/1000);
        good.setUpdTime(System.currentTimeMillis()/1000);
        good.setIsDel("0");
        spGoodMapper.insert(good);
    }

    public SpGoods findgood(int id) {
        SpGoods goods = spGoodMapper.selectById(id);
        return goods;
    }

    public void up(int id, SpGoods good) {
        good.setGoodId(id);
        spGoodMapper.updateById(good);
    }

    public void del(int id) {
        spGoodMapper.deleteById(id);
    }
}
