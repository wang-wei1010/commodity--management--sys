package com.example.demo.zjitc.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 管理员表
 * </p>
 *
 * @author Wang
 * @since 2022-01-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SpManager对象", description="管理员表")
public class SpManager implements Serializable {

    @TableId(value = "mg_id",type = IdType.AUTO)
    private Integer id;
    @TableField(value = "mg_name")
    private String username;
    @TableField(value = "mg_pwd")
    private String password;
    @TableField(value = "mg_mobile")
    private String mobile;
    @TableField(value = "mg_state")
    private Integer type;
    @TableField(value = "mg_email")
    private String email;
    @TableField("mg_time")
    private Integer mg_time;
    @TableField("role_id")
    private Integer role_id;

    public void setMgTime(Integer mg_time) {
        this.mg_time = mg_time;
    }

    public void setRoleId(Integer role_id) {
        this.role_id = role_id;
    }

}

