package com.example.demo.zjitc.server.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
@TableName("sp_category")
public class Category {
    @TableId(value = "cat_id",type = IdType.AUTO)
    private int cat_id;
    private String cat_name;
    private int cat_pid;
    private int cat_level;
    private int cat_deleted;
    private String cat_icon;
    private String cat_src;
    @TableField(exist = false)
    private List<Category> children;

    public void setCatId(int cat_id) {
        this.cat_id = cat_id;
    }

    public void setCatName(String cat_name) {
        this.cat_name = cat_name;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }

    public void setCatPid(int cat_pid) {
        this.cat_pid = cat_pid;
    }

    public void setCatLevel(int cat_level) {
        this.cat_level = cat_level;
    }

    public void setCatDeleted(int cat_deleted) {
        this.cat_deleted = cat_deleted;
    }

    public void setCatIcon(String cat_icon) {
        this.cat_icon = cat_icon;
    }

    public void setCatSrc(String cat_src) {
        this.cat_src = cat_src;
    }
}