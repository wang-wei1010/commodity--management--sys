package com.example.demo.zjitc.server.controller;

import com.example.demo.zjitc.server.common.reponse.ResponseResult;
import com.example.demo.zjitc.server.pojo.Menus;
import com.example.demo.zjitc.server.service.impl.MenuServiceimpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Author: Wang
 * Date: 2022/1/3 18:47
 * 权限管理控制器
 */
@RestController
@RequestMapping("/server")
public class MenuController {

     @Autowired
     private MenuServiceimpl serviceimpl;

    @GetMapping("/menu")
    @ResponseBody
    public ResponseResult getMenu()
    {
        List<Menus> list =  serviceimpl.getMenu();
        ResponseResult<List<Menus>> result = new ResponseResult<>() ;
        if (list!=null)
        {
            result.setSuccess("获取菜单列表成功",list);
            return result;
        }
        else {
            result.setSuccess("获取菜单列表失败");
            return result;
        }
    }
}
