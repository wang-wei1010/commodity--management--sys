package com.example.demo;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;

/**
 * Author: Wang
 * Date: 2021/12/15 9:56
 * 代码生成器
 */
public class WangCode {

  public static void main(String[] args) {

    //构建一个代码生成器对象
    AutoGenerator mpg = new AutoGenerator();

    //1.配置策略
    GlobalConfig gc = new GlobalConfig();
    String projectPath = System.getProperty("user.dir");
    gc.setOutputDir(projectPath+"/src/main/java");
    gc.setAuthor("Wang");
    gc.setOpen(false);//是否打开资源管理器
    gc.setFileOverride(false);//是否覆盖原有资源
    gc.setServiceName("%sService");//去Service的I前缀
    gc.setIdType(IdType.ID_WORKER);
    gc.setDateType(DateType.ONLY_DATE);
    gc.setSwagger2(true);
    mpg.setGlobalConfig(gc);

    //2.设置数据源
    DataSourceConfig dsc = new DataSourceConfig();
    dsc.setUrl("jdbc:mysql://localhost:3306/mydb?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone =Asia/Shanghai");
    dsc.setDriverName("com.mysql.cj.jdbc.Driver");
    dsc.setUsername("root");
    dsc.setPassword("password");
    dsc.setDbType(DbType.MYSQL);
    mpg.setDataSource(dsc);

    //3.包的设置
    PackageConfig pc = new PackageConfig();
    pc.setModuleName("server");
    pc.setParent("com.example.demo.zjitc");
    pc.setEntity("pojo");
    pc.setMapper("mapper");
    pc.setService("service");
//    pc.setController("controller");
    mpg.setPackageInfo(pc);


    //4.策略配置
    StrategyConfig strategy = new StrategyConfig();
    strategy.setInclude("sp_permission_api");
    strategy.setNaming(NamingStrategy.underline_to_camel);
    strategy.setColumnNaming(NamingStrategy.underline_to_camel);
    strategy.setEntityLombokModel(true);
   // strategy.setLogicDeleteFieldName("deleted");//逻辑删除

    //自动填充配置
//    TableFill gmtCreate = new TableFill("gmt_create", FieldFill.INSERT);
//    TableFill gmtModified = new TableFill("gmt_modified",FieldFill.INSERT_UPDATE);
//    ArrayList<TableFill> tableFills = new ArrayList<>();
//    tableFills.add(gmtCreate);
//    tableFills.add(gmtModified);
//    strategy.setTableFillList(tableFills);

    //乐观锁
    strategy.setVersionFieldName("version");

    strategy.setRestControllerStyle(true);
    strategy.setControllerMappingHyphenStyle(true);
    mpg.setStrategy(strategy);


    mpg.execute();

  }

}
