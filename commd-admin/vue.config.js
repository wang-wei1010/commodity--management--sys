module.exports = {
    lintOnSave: false,
	 css: {
    extract: false
  },

  devServer: {
    proxy: {
        '/server': {
            target: 'http://127.0.0.1:8087',//后端接口地址
            changeOrigin: true,//是否允许跨越
            pathRewrite: {
                '^/server': '/server'//重写,
            }
        }
    }
}




}


