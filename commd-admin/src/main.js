import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
//导入全局样式表
import './assets/css/global.css'
//导入字体图标
import './assets/fonts/iconfont.css'
//导入axios
import axios from 'axios'

//导入table（表格）插件
import TreeTable from 'vue-table-with-tree-grid'
//导入副文本编辑器 
import VueQuillEditor from 'vue-quill-editor'
//样式的导入
import 'quill/dist/quill.core.css' 
import 'quill/dist/quill.snow.css' 
import 'quill/dist/quill.bubble.css'
//导入进度条插件
import NProgress from 'nprogress'
import 'nprogress/nprogress.css' 
//导入页面背景效果
import VueParticles from 'vue-particles'  


//配置请求的根路径
//axios.defaults.baseURL='http://localhost:8087/'
// axios.defaults.headers.post['Content-Type'] = 'text/plain'
//配置相应的的拦截器
axios.interceptors.response.use(function (response) {    
  NProgress.done()
//  window.sessionStorage.setItem("token",response.headers.token);
  //sessionStorage.setItem('token',response.headers.token)

  return response;
}, function (error) {    
  return Promise.reject(error);
});


//配置请求的拦截器
axios.interceptors.request.use(config=>{
   NProgress.start()
   config.headers.token = window.sessionStorage.getItem("token");
   if (config.method === "post") {
    config.data = { unused: 0 }; // 这个是关键点，加入这行就可以了,解决get,请求添加不上Content-Type
}
  
       return config;
})
//为原型对象添加一个属性
Vue.prototype.$http = axios

Vue.config.productionTip = false

//将其注册为全局组件
Vue.component("tree-table",TreeTable)
//将副文本编辑器注册为全局可用的组件
Vue.use(VueQuillEditor)

//页面背景全局组件
Vue.use(VueParticles)


//全局过滤器
Vue.filter("dateFormat",function(originVal){
  const dt = new Date(originVal);
  
  const y = dt.getFullYear();
  const m = (dt.getMonth()+1+'').padStart(2,'0')
  const d = (dt.getDate()+'').padStart(2,'0')
  const hh = (dt.getHours()+'').padStart(2,'0')
  const mm = (dt.getMinutes()+'').padStart(2,'0')
  const ss = (dt.getSeconds()+'').padStart(2,'0')
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
