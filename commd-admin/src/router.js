import Vue from 'vue'
import Router from 'vue-router'
//导入登录组件
import Login from './components/Login.vue'
//导入主页面
import Home from './components/Home.vue'
//导入欢迎页面
import Welcome from './components/Welcome.vue'
//导入用户页面
import Users from './components/user/Users.vue'
//导入用户权限列表页面
import Rights from './components/power/Rights.vue'
//导入角色页面
import Roles from './components/power/Roles.vue'
//导入商品页面
import Cate from './components/goods/Cate.vue'
//导入商品参数修改页面
import Params from './components/goods/Params.vue'
//导入商品列表页面
import GoodsList from './components/goods/List.vue'
//导入商品添加页面
import Add from './components/goods/Add.vue'
//导入订单页面
import Oreder from './components/order/Order.vue'
//导入数据报表界面
import Report from './components/report/Report.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {path:'/',redirect:'/login'},
    {path:'/login',component:Login},
    {path:'/home',component:Home,
     redirect:'/welcome',
     children:[{path:'/welcome',component:Welcome},{path:'/users',component:Users},
                {path:'/rights',component:Rights},{path:'/roles',component:Roles},                
                {path:'/categories',component:Cate},{path:"/params",component:Params},
                {path:"/goods",component:GoodsList},{path:"/goods/add",component:Add},
                {path:"/orders",component:Oreder},{path:"/reports", component:Report}
              ]
  }
  

  ]
})

//挂载路由导航守卫
// router.beforeEach((to,from,next)=>{
//    //to将要访问的路径
//    //from 代表从哪个路径跳转而来
//    //next 是一个函数 表示放行
//     if(to.path=='/login') return next()

//     //获取token
//     const tokenStr = window.sessionStorage.getItem("token")
//     if(!tokenStr) return next("/login")
//     next()
// })

export default router
